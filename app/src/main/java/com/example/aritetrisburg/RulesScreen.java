package com.example.aritetrisburg;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class RulesScreen extends AppCompatActivity {
    Button toHomeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules_screen);

        toHomeButton = (Button) findViewById(R.id.backButton);

        toHomeButton.setOnClickListener(v -> {
            Intent intent = new Intent(RulesScreen.this,HomeScreen.class);
            startActivity(intent);
        });
    }
}