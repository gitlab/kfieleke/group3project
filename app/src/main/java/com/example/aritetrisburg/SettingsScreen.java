package com.example.aritetrisburg;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsScreen extends AppCompatActivity {
    Button toHomeButton;
    public SeekBar pointX, speedX;

    public int pointX_int;
    public int speedX_int;

    public int scoreMultiplier;
    public SharedPreferences sharedPreferencesPoint, sharedPreferencesSpeed;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_screen);

        toHomeButton = (Button) findViewById(R.id.btnSetting);
        pointX = (SeekBar) findViewById(R.id.seekPoint);
        speedX = (SeekBar) findViewById(R.id.seekSpeed);

        pointX.setMax(100);
        speedX.setMax(100);

        sharedPreferencesPoint = getPreferences(Context.MODE_PRIVATE);
        sharedPreferencesSpeed = getPreferences(Context.MODE_PRIVATE);


        pointX_int = sharedPreferencesPoint.getInt("last_position_point", 1);
        speedX_int = sharedPreferencesSpeed.getInt("last_position_speed", 1);

        pointX.setProgress(pointX_int);
        speedX.setProgress(speedX_int);


        pointX.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //System.out.println(store_input_point(progress));
                pointX_int = progress;
                System.out.println(pointX_int + " first");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                pointX_int = seekBar.getProgress();
                scoreMultiplier = pointX_int;
                System.out.println(pointX_int + " second");

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                SharedPreferences.Editor editor = sharedPreferencesPoint.edit();
                editor.putInt("last_position_point", seekBar.getProgress());
                editor.apply();
                pointX_int = seekBar.getProgress();
                scoreMultiplier = pointX_int;
                System.out.println(pointX_int + " third");


            }
        });

        speedX.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //System.out.println(store_input_point(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                SharedPreferences.Editor editor = sharedPreferencesSpeed.edit();
                editor.putInt("last_position_speed", seekBar.getProgress());
                editor.apply();
                speedX_int = seekBar.getProgress();
                System.out.println(speedX_int);
            }
        });

        toHomeButton.setOnClickListener(v -> {
            Intent intent = new Intent(SettingsScreen.this, HomeScreen.class);
            startActivity(intent);
            System.out.println(pointX_int);
        });
    }

    public int store_input_point(int i)
    {
        return i;
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        // Store the last position in shared preferences when the activity is destroyed
//        SharedPreferences.Editor editorPoint = sharedPreferencesPoint.edit();
//        editorPoint.putInt("last_position_point", pointX.getProgress());
//        editorPoint.apply();
//        System.out.println(pointX_int);
//
//
//        SharedPreferences.Editor editorSpeed = sharedPreferencesSpeed.edit();
//        editorSpeed.putInt("last_position_speed", pointX.getProgress());
//        editorSpeed.apply();
//    }
}