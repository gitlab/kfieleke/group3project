package com.example.aritetrisburg;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class HomeScreen extends AppCompatActivity {
    Button toPlayButton, toSettingsButton, toRulesButton, toScoreLeaderBoardButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        toPlayButton = (Button) findViewById(R.id.playButton);
        toSettingsButton = (Button) findViewById(R.id.settingsButton);
        toRulesButton = (Button) findViewById(R.id.rulesButton);
        toScoreLeaderBoardButton = (Button) findViewById(R.id.scoreLeaderboardButton);

        toPlayButton.setOnClickListener(v -> {
            Intent intent1 = new Intent(HomeScreen.this, EnterNameScreen.class);
            startActivity(intent1);
        });

        toSettingsButton.setOnClickListener(v -> {
            Intent intent2 = new Intent(HomeScreen.this, SettingsScreen.class);
            startActivity(intent2);
        });

        toRulesButton.setOnClickListener(v -> {
            Intent intent3 = new Intent(HomeScreen.this, RulesScreen.class);
            startActivity(intent3);
        });

        toScoreLeaderBoardButton.setOnClickListener(v -> {
            Intent intent4 = new Intent(HomeScreen.this, ScoreLeaderboardScreen.class);
            startActivity(intent4);
        });
    }
}