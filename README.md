# Group3Project (AriTetrisburg)



## Instructions for Installation

1. Create a new android studio project from Version Control System (i.e. 'Get from VCS').
2. Paste in this repo's clone link: https://agile.bu.edu/gitlab/jilin/group3project.git
3. While waiting for all files to load, make sure you have an emulator set up; this demo uses the Nexus 5X with API 30.
4. When all files have loaded, hit the 'Run' button @ the top right toolbar.
5. Enjoy the game on your emulator!
